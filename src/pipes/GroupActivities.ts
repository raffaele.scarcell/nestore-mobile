import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
@Pipe({
  name: 'groupActivities'
})
export class GroupActivities implements PipeTransform {

  public transform(value: any, param: string = null): any {
    console.log(value);
    let groupActivities = [];
    for(let activity of value){
      let c = groupActivities.find(x=>x.title == activity.title);
      if(!c){
        c = {'title':activity.title, 'activities':[]};
        groupActivities.push({'title':activity.title, 'activities':[]})
        c = groupActivities.find(x=>x.title == activity.title);
      }
      c.activities.push(activity);
    }
    console.log(groupActivities);
    return groupActivities;
  }

}
