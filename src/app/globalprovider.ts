import {EventEmitter, Injectable} from '@angular/core';
import {Subject} from 'rxjs/Rx';
import { ENVIRONMENT } from "../environement.config";
import {Storage} from "@ionic/storage";
import {APIService} from "../chat/services/api";
import {LogsService} from "../providers/logsService";

@Injectable()
export class GlobalvarProvider {
  public _isChatOpen : boolean = false;
  public chatCreated = false;

  public environment = ENVIRONMENT;
  public onChatOpen = new EventEmitter();

  public active_tab:any;
  private isSleeping = false;
  public lang;
  public id;
  public changeTab = new EventEmitter();
  public isActivityOpen = false;
  public chatPhoto;
  public firstname;
  public token;
  public textNotificationTitle;

  public selectedTab;

  public startedActivity;
  public lastTestDate;

  public startQuestionnaireEvent = new Subject();

  public loginEvent = new Subject();
  public logoutEvent = new Subject();

  public profile;
  public waterUpdate = new Subject();

  public startGame = new Subject();

  public reloadActivitiesSubject = new Subject();
  public reloadChartSubject = new Subject();

  public reloadProposedActivitiesSubject = new Subject();
  public proposedActivitiesLoaded;

  public blur = false;
  public tuto;
  public selectedPathway = null;

  public selectedDomain = null;

  public selectedGame = null;

  public currentGame = null;
  public quit = null;

  public canBack = true;

  public error;
  public internalError;

  public message;
  public currentQuestionnaire;

  public click2weeks = false;
  public processLoading = false;

  public version;

  back = new Subject();


  get isChatOpen(): boolean {
    return this._isChatOpen;
  }

  constructor(private storage: Storage, private logs: LogsService){
    this.active_tab=null;
  }

  reloadActivities(isPathway = null){
    this.reloadActivitiesSubject.next(isPathway);
  }

  reloadChart(options){
    this.reloadChartSubject.next(options);
  }

  reloadProposedActivities(){
    this.reloadProposedActivitiesSubject.next();
  }

  openChat(value = true){
      this.chatCreated = true;
      if(value){
        this.logs.start('chat');
        if(this.active_tab==0){
          this.logs.stopAndSend('home')
        }
        else if(this.active_tab==1){
          this.logs.stopAndSend('activity')
        }
        else if(this.active_tab==2) {
          this.logs.stopAndSend('charts')
        }
        else if(this.active_tab==3) {
          this.logs.stopAndSend('profile')
        }
      }
      else if(this.isChatOpen) {
        this.logs.stopAndSend('chat');
        if(this.active_tab==0){
          this.logs.start('home')
        }
        else if(this.active_tab==1){
          this.logs.start('activity')
        }
        else if(this.active_tab==2) {
          this.logs.stopAndSend('charts')
        }
        else if(this.active_tab==3) {
          this.logs.stopAndSend('profile')
        }
      }
      this._isChatOpen = value;
  }

  beginGame(game, taskId=null, messageId=null, messageIndex=null){
    if(this.profile.stage == 'baseline'){

    }
    this.selectedDomain='cognitive';
    this.currentGame = {name:game, taskId: taskId, messageId:messageId, messageIndex:messageIndex};
  }

  setActiveTab(val){
    this.selectedTab=val;
    console.log(val);
    if(this.active_tab===0 && val!==0){
      this.logs.stopAndSend('home')
    }
    if(this.active_tab===1 && val!==1){
      this.logs.stopAndSend('activity')
    }
    if(this.active_tab===2 && val!==2){
      this.logs.stopAndSend('charts')
    }
    if(this.active_tab===3 && val!==3){
      this.logs.stopAndSend('profile')
    }
    this.active_tab=val;
    this.changeTab.next(val);
    if(val===0){
      this.logs.start('home')
    }
    if(val===1){
      this.logs.start('activity')
    }
    if(val===2){
      this.logs.start('charts')
    }
    if(val===3){
      this.logs.start('profile')
    }
  }

  setIsSleeping(val){
    this.isSleeping=val;
  }
  getIsSleeping(){
    return this.isSleeping;
  }
  setId(val){
    this.id=val;
  }
  getId(){
    return this.id;
  }
  getToken(){
    return this.token;
  }
  setToken(token){
    this.token=token;
  }



}
