import { Injectable } from "@angular/core";
import { HTTP } from "@ionic-native/http";
import { Observable } from "rxjs/Observable";
import { APIS } from "../register/REGISTER_CONFIG";
import { Observer } from "rxjs";

@Injectable()
export class LoginRegisterProvider {
  constructor(public http: HTTP) {}

  public register(data): Observable<any> {
    return Observable.create((observer: Observer<any>) => {
      let registerData = {
        credentialType: "password",
        emailVerified: false,
        captchaResponse: data.captchaResponse,
        username: data.form.userName.value,
        firstName: data.form.firstName.value,
        lastName: data.form.lastName.value,
        email: data.form.email.value,
        credentialValue: data.form.password_group.controls.password.value,
        birthday: data.form.birthday.value,
        gender: data.form.gender.value,
        country: data.form.country.value,
        defaultTimeZone: data.form.defaultTimeZone.value,
        language: data.form.language.value,
        suffix: data.form.suffix.value,
        title: data.form.title.value,
        screenName: data.form.screenName.value,
        middleName: data.form.middleName.value,
        description: data.form.description.value,
        jobTitle: data.form.job.value,
        pilotSite: data.form.pilotSite.value
      };

      this.http.setDataSerializer("urlencoded");
      this.http.post(`${APIS.nestoreRegister.url}${APIS.nestoreRegister.endpoints.register}`, registerData, APIS.nestoreRegister.headers).then(
        result => {
          observer.next(result);
          observer.complete();
        },
        err => {
          observer.error(err);
          observer.complete();
        }
      );
    });
  }
}
