import {Component, Input, SimpleChanges} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import {APIService} from '../../../chat/services/api';

@Component({
  selector: 'nestore-energy-chart',
  templateUrl: 'energy-chart.html'
})
export class EnergyChartComponent {
  @Input() timeframe;
  @Input() value = 0;
  @Input() intake = null;
  @Input() pathway = null;
  @Input() total = 2000;
  @Input() data;

  @Input() expenditure = null;

  showdetails = false;

  ngOnChanges(change){
    if (change.data) {
      console.log(change);
    }
      if(this.timeframe==='day') {
      if (change.data) {
        if (this.data) {
          this.expenditure = this.data.calories;
        } else {
          this.expenditure = 0;
        }
      }
    }
  }

}
