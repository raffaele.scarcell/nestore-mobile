import {Component, EventEmitter, Input, Output} from '@angular/core';
import { Camera } from '@ionic-native/camera';

import { APIService } from '../../chat/services/api';
import { SubmitDishPage } from '../submit-dish/submit-dish';
import {TranslateService} from '@ngx-translate/core';
import {GlobalvarProvider} from '../../app/globalprovider';
import {MessageProvider} from "../../chat/services/message-provider";


@Component({
  selector: 'page-cognitive',
  templateUrl: 'cognitive.html',
})
export class CognitivePage {
@Output() onClose = new EventEmitter();
@Output() canBack = new EventEmitter();

  @Input() game = null;
  @Output() gameChange = new EventEmitter();
  clicked = false;
  options;
  startDate;

  constructor(
              private api: APIService,
              public translate: TranslateService,
              public globalProvider: GlobalvarProvider,
              public messageProvider: MessageProvider,

  ) {

  }

  ngOnChanges(change){
    if(change.game && this.game){
      this.startDate = new Date();
      this.startGame();
    }
  }

  close(){
    this.onClose.emit()
  }

  startGame(){
    if(this.game.name=='updating'){
      this.api.getGameInfos('numericalupdatingtask').then((options)=>{
        this.options = options;
        if(this.game.taskId) {
          this.options.taskId = this.game.taskId;
        }
        this.options.mode = this.globalProvider.profile.stage;
        if(this.globalProvider.profile.stage==='intervention'){
          this.options.sessionStartDate = this.startDate
        }
        this.canBack.emit(false);
      },()=>{
        this.canBack.emit(true);
        this.options = 'error';
      })
    }
    else if(this.game.name=='nback'){
      this.api.getGameInfos('nback').then((options)=>{
        this.options = options;
        this.canBack.emit(false);
      },()=>{
        this.canBack.emit(true);
        this.options = 'error';
      })
    }
    else if(this.game.name=='digit_span'){
      this.options = {};
      this.options.messageId = this.game.messageId;
      this.options.messageIndex = this.game.messageIndex;
      this.canBack.emit(false);
    }
  }

  saveMessage(data){
    console.log('savemessage');
    this.messageProvider.saveData({score:data.score}, data.messageId, data.messageIndex)
  }

  endGame() {
      console.log('endGame');
      if (this.options.mode === 'intervention' && this.game.name !== 'digit_span') {
        this.globalProvider.reloadActivities();
        this.globalProvider.setActiveTab(2);
        setTimeout(() => {
          this.globalProvider.reloadChart({timeframe: 'week', domain: 'cognitive'});
          this.globalProvider.openChat(false)
        }, 1);

      }
    this.game = null;
    this.options = null;
      this.gameChange.emit(this.game);
      this.canBack.emit(true);
      this.close();
  }
}
