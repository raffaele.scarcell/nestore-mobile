import { HistoryEvent } from "./history-event";
import { NativeStorage } from "@ionic-native/native-storage";

export class NutritionEvent extends HistoryEvent {

    photo: String = "";

    constructor(
        domain: string,
        timestamp: Date,
        ts_str: string,
        id: string,
        public dish: string,
        public meal: string,
        public hasPhoto: boolean,
        public nativeStorage = new NativeStorage()
    ) {
        super(domain, timestamp, ts_str, id);
        if (hasPhoto) {
            this.getPhoto();
        }
    }

    show() {
        console.log(this);
    }
    
    getPhoto() {
        return this.nativeStorage.getItem(this.id).then(
        (success) => {
            console.log("got photo: " + this.id, success);
            this.photo =  success.photo;
        },
        (error) => {
            console.log("couldn't get photo: " + this.id, error);
        });
    }

}
