import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';

import { SubmitDishPage } from '../submit-dish/submit-dish';
import { TranslateService } from '@ngx-translate/core';
import { ApiProvider } from '../../providers/api/api';
import { NutritionSelectPage } from '../nutrition-select/nutrition-select';
import { NativeStorage } from '@ionic-native/native-storage';
import {APIService} from '../../chat/services/api';
import {GlobalvarProvider} from '../../app/globalprovider';


@Component({
  selector: 'page-social',
  templateUrl: 'social.html',
})
export class SocialPage {
  @Output() onClose = new EventEmitter();
  @Output() canBack = new EventEmitter();
  clicked = false;

  constructor(private api: APIService, private globalvarProvider: GlobalvarProvider) {
  }

  close() {
    this.onClose.emit()
  }

}
