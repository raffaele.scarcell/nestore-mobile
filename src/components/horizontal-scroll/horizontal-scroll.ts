import {
  Component,
  ContentChildren, ElementRef,
  EventEmitter,
  HostListener,
  Input,
  Output,
  SimpleChanges, TemplateRef,
  ViewChild
} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';

/**
 * Generated class for the AllComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'horizontal-scroll',
  templateUrl: 'horizontal-scroll.html'
})

export class HorizontalScrollComponent {
  @Input() page;
  @Output() pageChange = new EventEmitter();
  @Output() pagesChange = new EventEmitter();
  @Input() selectPage;
  @Output() selectPageChange = new EventEmitter();
  @ViewChild('scrollView') scrollView;

  @Input() isMoving = false;
  @Output() isMovingChange = new EventEmitter();

  isInit;

  @Input() articles;



  ngAfterViewInit(){
    this.isInit = true;
    this.scrollView.nativeElement.addEventListener('touchstart', ()=>{this.isTouching=true}, {passive: true});
    this.scrollView.nativeElement.addEventListener('touchend', ()=>{this.isTouching=false;this.f()}, {passive: true});
    this.pages = this.articles.length;
    this.pagesChange.emit(this.pages);
    this.pageChange.emit(0);
    this.selectPageChange.emit(0);

  }

  ngOnChanges(change){
    if(change.selectPage && this.isInit){
      this.f(this.selectPage);
    }
  }

  pages = 0;
  isScrolling;
  isTouching;

  scrollHandler(event) {
    // Clear our timeout throughout the scroll
    window.clearTimeout( this.isScrolling );

      let p = Math.ceil(Math.round(this.scrollView.nativeElement.scrollLeft/(this.scrollView.nativeElement.children[0].children[0].clientWidth / 2))/2);
      if(p!==this.page) {
        this.page=p;
        this.selectPage=p;
        this.pageChange.emit(this.page);
        console.log('test');
        this.selectPageChange.emit(this.selectPage);
      }
    if (!this.isTouching) {
      console.log('scroll without finger');
      this.isMoving = true;
      this.isMovingChange.emit(this.isMoving);
      // Set a timeout to run after scrolling ends
      this.isScrolling = setTimeout(() => {
        // Run the callback
        console.log('Scrolling has stopped.');
        this.isMoving=false;
        this.isMovingChange.emit(this.isMoving);

        if (this.scrollView.nativeElement.scrollLeft > 0) {
          let child = Math.ceil(Math.round(this.scrollView.nativeElement.scrollLeft/(this.scrollView.nativeElement.children[0].children[0].clientWidth / 2))/2);

          this.scrollView.nativeElement.children[0].children[child].scrollIntoView({
            behavior: "smooth",
            block: "start",
            inline: "start",
          });
          this.isScrolling=null
        }
      }, 200);


    }
  }

  f(page=null) {
    let child;
    if(!page) {
       child = Math.ceil(Math.round(this.scrollView.nativeElement.scrollLeft / (this.scrollView.nativeElement.children[0].children[0].clientWidth / 2)) / 2);
    }
    else {
      child = page;
      console.log(page);
    }
    console.log(child);

      this.scrollView.nativeElement.children[0].children[child].scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "start"
      });

  }
}
