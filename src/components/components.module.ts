import { NgModule } from '@angular/core';

import { AddActionFabComponent } from './add-action-fab/add-action-fab';
import { IonicModule } from 'ionic-angular';
import { NestoreHeaderComponent } from './nestore-header/nestore-header';
import {MealPickerModule} from './meal-picker/meal-picker.module';
import {MealPickerSearchComponent} from './meal-picker/meal-picker-search.component';
import {MealPickerComponent} from './meal-picker/meal-picker.component';
import { NestoreProgressBarComponent } from './nestore-progress-bar/nestore-progress-bar';
import {NumericalKeyboardComponent} from './numeric_keyboard/numerical-keyboard';
import {HttpClient} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, ENVIRONMENT.COACH_SERVER + 'translation/mobile/');
}

import {MealPickerSearchCustomComponent} from './meal-picker-search-custom/meal-picker-search-custom';
import {LonelinessComponent} from './charts/loneliness/loneliness.component';
import {SpinnerComponent} from './spinner/spinner.component';
import {NestoreFilterHeaderComponent} from './nestore-filter-header/nestore-filter-header.component';
import {NestoreMultichoiceComponent} from './nestore-multichoice/nestore-multichoice.component';
import {DatePipeProxy} from '../pipes/DatePipeProxy';
import {ActivityCardComponent} from './activity-card/activity-card.component';
import { MemoryAccuracyComponent } from './charts/memory-accuracy/memory-accuracy';
import {ChartsModule} from "ng2-charts";
import { TypeChartComponent } from './charts/type-chart/type-chart';
import { AllComponent } from './charts/all/all';
import { LocalComponent } from './charts/local/local';
import { MemoryFailuresComponent } from './charts/memory-failures/memory-failures';
import { LonelinessMonthComponent } from './charts/loneliness-month/loneliness-month';
import {ClickOutsideModule} from 'ng-click-outside';
import {NestoreNumberPickerComponent} from './nestore-number-picker/nestore-number-picker.component';
import {NestoreScaleComponent} from './nestore-scale/nestore-scale.component';
import {NestoreWaterGlassComponent} from './nestore-water-glass/nestore-water-glass.component';
import {NestorePieChartComponent} from './nestore-pie-chart/nestore-pie-chart';
import {WaterChartComponent} from './charts/water-chart/water-chart';
import {NestoreCircleComponent} from './nestore-circle/nestore-circle';
import {NestoreSingleChoiceComponent} from './nestore-singlechoice/nestore-singlechoice.component';
import {ProgressChartComponent} from './charts/progress-chart/progress-chart';
import {NestoreBMIComponent} from './charts/bmi-chart/nestore-bmi';
import {Omega3ChartComponent} from './charts/omega3-chart.html/omega3-chart';
import {PathwayChoiceComponent} from '../pathway-choice/pathway-choice.component';
import {MyscoreChartComponent} from './charts/myscore-chart/myscore-chart';
import {StructuredActivityChartComponent} from './charts/structured-activity-chart/structured-activity-chart';
import {NestoreDoublePieChartComponent} from './nestore-double-pie-chart/nestore-double-pie-chart';
import {EnergyChartComponent} from './charts/energy-chart/energy-chart';
import {NestoreGameCardComponent} from './nestore-game-card/nestore-game-card.component';
import {NestoreCalendarComponent} from './nestore-calendar/nestore-calendar.component';
import {NestoreBorgScaleComponent} from './nestore-borg-scale/nestore-borg-scale.component';
import {PhysicalPreferencesChoiceComponant} from "../physical-preferences-choice/physical-preferences-choice.componant";
import {ActivitySchedulingComponent} from "./activity-scheduling/activity-scheduling.component";
import {ActivityStartComponent} from "./activity-start/activity-start.component";
import {HorizontalScrollComponent} from "./horizontal-scroll/horizontal-scroll";
import {NavigationDotsComponent} from "./navigation-dots/navigation-dots";
import {UnStructuredActivityChartComponent} from "./charts/unstructured-activity-chart/unstructured-activity-chart";
import {SedentarinessChartComponent} from "./charts/sedentariness-chart/sedentariness-chart";
import {NestoreGridCirclesComponent} from "./nestore-grid-circles/nestore-grid-circles";
import {DimensionalEmotionsChart} from "./charts/dimensional-emotions-chart/dimensional-emotions-chart";
import {DiscreteEmotionsChart} from "./charts/discrete-emotions-chart/discrete-emotions-chart";
import {ListMealPickerComponent} from "./meal-picker/list-meal-picker.component";
import {NestoreActivityCheckComponent} from "./nestore-activity-check/nestore-activity-check";
import {SleepChartComponent} from "./charts/sleep-chart/sleep-chart";
import {NestoreTimeChart} from "./nestore-time-chart/nestore-time-chart";
import {NestoreErrorComponent} from "./nestore-error/nestore-error";
import {NestorePopupComponent} from "./nestore-popup/nestore-popup";
import {StrengthScoreChartComponent} from "./charts/strength-score-chart/strenght-score-chart";
import {ActivitySelectComponent} from "./activity-select/activity-select";
import {PathwayForDomain} from "../pipes/pathwayForDomain";
import {ChatMessageComponent} from "./chat/chat-message/chat-message";
import {ChatQuestionnaireComponent} from "./chat/chat-questionnaire/chat-questionnaire.component";
import {NestoreToggleComponent} from "./nestore-toggle/nestore-toggle.component";
import {LoadDirective} from "../pipes/LoadDirective";
import {NutrientChartComponent} from "./charts/nutrient-chart/nutrient-chart";
import {TransitionEndDirective} from "../pipes/TransitionEndDirective";
import {NestoreLineChartComponent} from "./graphs/nestore-line-chart/nestore-line-chart";
import {NestoreBarChartComponent} from "./graphs/nestore-bar-chart/nestore-bar-chart";
import {NestoreBarChartOldComponent} from "./nestore-bar-chart/nestore-bar-chart";
import {ENVIRONMENT} from "../environement.config";
import {NumberToArrayPipe} from "../pipes/NumberToArrayPipe";
import {NestoreWearableInfoComponent} from "./nestore-wearable-info/nestore-wearable-info";
import {NestorePathwayInfoComponent} from "./nestore-pathway-info/nestore-pathway-info";
import {NestoreTutorialComponent} from "./nestore-tutorial/nestore-tutorial";
import {TranslateDataPipe} from "../pipes/TranslateData";

@NgModule({
  declarations: [
    AddActionFabComponent,
    NestoreHeaderComponent,
    NestoreBMIComponent,
    NumericalKeyboardComponent,
    NestoreProgressBarComponent,
    NestoreToggleComponent,
    LonelinessComponent,
    NestoreFilterHeaderComponent,
    NestoreMultichoiceComponent,
    NestoreSingleChoiceComponent,
    NestoreNumberPickerComponent,
    NestoreScaleComponent,
    NestoreBorgScaleComponent,
    NestoreWaterGlassComponent,
    ActivityCardComponent,
    DatePipeProxy,
    PathwayForDomain,
    MemoryAccuracyComponent,
    TypeChartComponent,
    AllComponent,
    LocalComponent,
    MemoryFailuresComponent,
    LonelinessMonthComponent,
    NestorePieChartComponent,
    NestoreDoublePieChartComponent,
    NestoreCircleComponent,
    ProgressChartComponent,
    WaterChartComponent,
    EnergyChartComponent,
    Omega3ChartComponent,
    PathwayChoiceComponent,
    PhysicalPreferencesChoiceComponant,
    MyscoreChartComponent,
    StrengthScoreChartComponent,
    NestoreGameCardComponent,
    StructuredActivityChartComponent,
    NestoreCalendarComponent,
    ActivitySchedulingComponent,
    ActivityStartComponent,
    HorizontalScrollComponent,
    NavigationDotsComponent,
    UnStructuredActivityChartComponent,
    SedentarinessChartComponent,
    NestoreGridCirclesComponent,
    NestoreBarChartComponent,
    NestoreBarChartOldComponent,
    DimensionalEmotionsChart,
    DiscreteEmotionsChart,
    SleepChartComponent,
    NestoreActivityCheckComponent,
    NestoreTimeChart,
    NestorePopupComponent,
    NestoreErrorComponent,
    ActivitySelectComponent,
    NutrientChartComponent,
    ChatQuestionnaireComponent,
    ChatMessageComponent,
    LoadDirective,
    TransitionEndDirective,
    NestoreLineChartComponent,
    NumberToArrayPipe,
    NestoreWearableInfoComponent,
    NestoreTutorialComponent,
    NestorePathwayInfoComponent,
    TranslateDataPipe
  ],
	imports: [
    TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient] //TODO : avoid to init again, possible PASS THIS IN FORROOT ?
        }
      }
    ),
		// CommonModule,
		IonicModule,
		MealPickerModule,
    ChartsModule,
    ClickOutsideModule
	],
  exports: [
    AddActionFabComponent,
    NestoreHeaderComponent,
    MealPickerSearchComponent,
    NestoreBMIComponent,
    MealPickerComponent,
    NumericalKeyboardComponent,
    NestoreProgressBarComponent,
    MealPickerSearchCustomComponent,
    SpinnerComponent,
    LonelinessComponent,
    NestoreToggleComponent,
    NestoreFilterHeaderComponent,
    NestoreMultichoiceComponent,
    NestoreSingleChoiceComponent,
    NestoreNumberPickerComponent,
    NestoreWaterGlassComponent,
    NestoreScaleComponent,
    NestoreBorgScaleComponent,
    ActivityCardComponent,
    DatePipeProxy,
    NumberToArrayPipe,
    MemoryAccuracyComponent,
    TypeChartComponent,
    AllComponent,
    LocalComponent,
    MemoryFailuresComponent,
    LonelinessMonthComponent,
    NestorePieChartComponent,
    NestoreDoublePieChartComponent,
    WaterChartComponent,
    EnergyChartComponent,
    ProgressChartComponent,
    NestoreCircleComponent,
    Omega3ChartComponent,
    MyscoreChartComponent,
    StrengthScoreChartComponent,
    PathwayChoiceComponent,
    PhysicalPreferencesChoiceComponant,
    NestoreGameCardComponent,
    StructuredActivityChartComponent,
    NestoreCalendarComponent,
    ActivitySchedulingComponent,
    ActivityStartComponent,
    HorizontalScrollComponent,
    NavigationDotsComponent,
    UnStructuredActivityChartComponent,
    SedentarinessChartComponent,
    NestoreGridCirclesComponent,
    NestoreBarChartComponent,
    NestoreBarChartOldComponent,
    DimensionalEmotionsChart,
    DiscreteEmotionsChart,
    ListMealPickerComponent,
    NestoreActivityCheckComponent,
    NestoreTimeChart,
    SleepChartComponent,
    NestorePopupComponent,
    NutrientChartComponent,
    NestoreErrorComponent,
    ActivitySelectComponent,
    PathwayForDomain,
    ChatQuestionnaireComponent,
    ChatMessageComponent,
    LoadDirective,
    TransitionEndDirective,
    NestoreLineChartComponent,
    NestoreWearableInfoComponent,
    NestoreTutorialComponent,
    NestorePathwayInfoComponent,
    TranslateDataPipe
  ]
})
export class ComponentsModule {}
