import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { IamClientProvider } from '../../providers/iam-client/iam-client';
import { TabsPage } from '../tabs/tabs';
import { GlobalvarProvider } from '../../app/globalprovider';
import { Keyboard } from '@ionic-native/keyboard';
import { TranslateService, TranslatePipe } from '@ngx-translate/core';
import {Storage} from "@ionic/storage";
//import { GooglePlus } from "@ionic-native/google-plus";


@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  @ViewChild("content") content;
  selectPage;
  page;
  pages;

  public keyb = false;
  public errorPassword = false;
  public credentials = {
    email: "",
    password: ""
  };
  public appLanguage;
  public isLogging = false;
  public splash = true;

  public isMoving;

  public signUp = false;

  constructor(public translate: TranslateService, public keyboard: Keyboard, public navCtrl: NavController, public globalvarProvider: GlobalvarProvider, public navParams: NavParams, public alert: AlertController, public iamClient: IamClientProvider, public el: ElementRef
              ,public storage: Storage) {

    this.keyboard.onKeyboardShow().subscribe(r => {
      this.keyb = false;
      this.content.scrollToBottom();
    });
    this.keyboard.onKeyboardHide().subscribe(r => {
      this.keyb = false;
    });


  }

  scrollToBottom(): void {
    const scrollPane: any = this.el.nativeElement.querySelector(".msg-container-base");
    scrollPane.scrollTop = scrollPane.scrollHeight;
  }

  login() {
    this.isLogging = true;
    this.storage.clear();
    this.iamClient.login(this.credentials).subscribe(
      next => {
        if (next) {
          this.isLogging = false;
          this.iamClient.getUser().then(user => {
            this.globalvarProvider.setId(user.sub);
            this.globalvarProvider.loginEvent.next();
          });
          this.errorPassword = false;
        } else {
          this.errorPassword = true;
        }
      },
      error => {
        this.isLogging = false;
        this.errorPassword = true;
        console.log(error);
      }
    );
  }

    next(){
    if(!this.isMoving) {
      console.log(this.page);
      if (this.selectPage === this.pages - 1) {
        this.splash = false;
      } else {
        this.selectPage++;
      }
    }
  }

}
