import {CLimits} from './CLimits';

export class Datas {
  private initValues = [];
  private mathOperations = []; //keep math operations
  private mathOperationsOrder = [];
  public userAns = []; // store user answers
  public correctAns = [];

  private nbTiles: number;
  private mathOpNum: number;
  private lims: CLimits;


  constructor(nbTiles: number, mathOpNumber: number, l: CLimits) {
    this.nbTiles = nbTiles;
    this.mathOpNum = mathOpNumber;
    this.lims = l;

    this.setInitValues();
    this.setMathOperationsOrder();
    this.setMathOperations();
    this.setCorrectAnswers();
    console.log(this.mathOperations);
  }

  getInitValue(index: number): number {
    return this.initValues[index];
  }

  getInitValues(): number[] {
    return [...this.initValues];
  }

  getMathOpBtn(index) {
    return this.mathOperationsOrder[index];
  }

  getMathOperationString(index) {
    let n = this.mathOperations[index];
    return (n >= 0 ? "+" : "") + n;
  }

  private setInitValues() {
    for (let i = 0; i < this.nbTiles; i++) {
      this.initValues[i] = this.lims.tileValuesLimits.getRandom();
    }
  }

  private setMathOperationsOrder() {
    let temp = [];
    for (let i = 0; i < this.nbTiles; i++) {
      temp.push(i);
    }

    for (let i = 0; i < this.mathOpNum;) {
      shuffle(temp);
      for (let j = 0; (j < this.nbTiles) && (i < this.mathOpNum); j++) {
        //avoid starting from same tile that you ended up when last in this loop
        if (i != 0 && j == 0 && this.nbTiles>1) {
          while (this.mathOperationsOrder[i - 1] === temp[0]) {
            shuffle(temp);
          }
        }
        this.mathOperationsOrder.push(temp[j]);
        i++;
      }
    }
  }


  private setMathOperations() {
    let mathAccepted = false;
    let tempResult = [...this.initValues];

    //end result after math operation should still be inside limits
    for (let i = 0; i < this.mathOpNum; i++) {
      mathAccepted = false;
      do {
        this.mathOperations[i] = this.lims.mathOperationsLimits.getRandomNonZero();

        if (this.lims.tileValuesLimits.check(tempResult[this.mathOperationsOrder[i]] + this.mathOperations[i])) {
          tempResult[this.mathOperationsOrder[i]] = tempResult[this.mathOperationsOrder[i]] + this.mathOperations[i];
          mathAccepted = true;
        }
      } while (!mathAccepted);
    }
  }


  private setCorrectAnswers() {
    this.correctAns = [...this.initValues];
    for(let i = 0; i< this.mathOperations.length; i++){
      this.correctAns[this.mathOperationsOrder[i] ] += this.mathOperations[i]
    }
  }

  storeAns(ans, index) {
    this.userAns[index] = ans;
  }

  checkAllAns() {
    for (let i = 0; i < this.nbTiles; i++) {
      if (!this.userAns[i] && this.userAns[i]!=0) {
        return false
      }
    }
    return true;
  }

  getPresentationString(index){
    //doScore(index);//computes correct answers too
    let temp = ""+this.getInitValue(index);
    for (let i = 0 ; i < this.mathOpNum; i++){
      if (this.mathOperationsOrder[i] === index){
        let n = this.mathOperations[i];
        if(n >= 0){
          temp = temp + " + " + n;
        }
        else{
          temp = temp + " - " + (n*-1);
        }
      }
    }
    temp = temp + " = " + this.correctAns[index];
    return temp;
  }

  getSuccessRateString(){
    let t = '';
    if(this.checkAllAns()){
      let s = 0;
      for(let i = 0; i < this.nbTiles; i++){
        if(this.correctAns[i] === this.userAns[i]){
          s++;
        }
      }
      t = s + "/" + this.nbTiles;
    }
    return t;
  }

  getScore(){
    let s = 0;
    for(let i = 0; i < this.nbTiles; i++){
      if(this.correctAns[i] === this.userAns[i]){
        s++;
      }
    }
    return s;
  }
}




/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
  let j, x, i;
  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = a[i];
    a[i] = a[j];
    a[j] = x;
  }
  return a;
}
