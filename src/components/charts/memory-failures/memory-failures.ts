import {Component, Input, SimpleChanges} from '@angular/core';
import {MemoryFailure} from "../../../models/memory-failure";

/**
 * Generated class for the MemoryFailuresComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'nestore-memory-failures',
  templateUrl: 'memory-failures.html'
})
export class MemoryFailuresComponent {
  showdetails = false;
  dayInitial = ['M','T','W','T','F','S','S']; //TODO: Use a library to get it in the four languages

  @Input() colorThresholds: object[] = [
    {'from': 0, 'to': 2, 'color': '#96E24B'},
    {'from': 3, 'to': 7, 'color': '#FFD938'},
    {'from': 8, 'to': 10, 'color': '#FD6E5B'},
  ];
  @Input() memoryFailuresStats: MemoryFailure[][] = [
    [{dayOfWeek: 0, value: 2}, {dayOfWeek: 3, value: 8}, {dayOfWeek: 6, value: 3}],
  ];
  @Input() defaultColor: string = '#F0F0F0';


  dataset: {dayOfWeek:number, value:number, color: string}[][];
  constructor() {
    this.computeChart();
  }

  ngOnChanges(changes: SimpleChanges) {
    if(this.memoryFailuresStats) {
      this.computeChart();
    }
  }


  computeChart() {
    this.dataset = [];
    console.log(this.memoryFailuresStats);
    for(let i = 0; i < this.memoryFailuresStats.length; i++) {
      let weekDataset = [];
      for(let j = 0; j < this.dayInitial.length; j++) {
        let elt = null;
        for(let k = 0; k < this.memoryFailuresStats[i].length; k++) {
          if(j == this.memoryFailuresStats[i][k].dayOfWeek) {
            elt = this.memoryFailuresStats[i][k];
            break;
          }
        }
        if(elt != null) {
          let color: string = null;
          for(let k = 0; k < this.colorThresholds.length; k++) {
            if(elt.value===null){
              color = this.defaultColor;
            }
            else if(elt.value >= this.colorThresholds[k]['from'] && elt.value <= this.colorThresholds[k]['to']) {
              color = this.colorThresholds[k]['color'];
            }
          }
          weekDataset.push({dayOfWeek: elt.dayOfWeek,
            value: {total:elt.value}, color: color
          });
        } else {
          weekDataset.push({dayOfWeek: j,
            value: null, color: this.defaultColor
          });
        }
      }
      console.log('week', weekDataset);
      this.dataset.push(weekDataset);
      console.log('final', this.dataset);
    }
  }

}
