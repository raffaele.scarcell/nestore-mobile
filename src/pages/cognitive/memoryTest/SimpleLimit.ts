export class SimpleLimit {
  upperBound : number;
  lowerBound : number;

  constructor(lower: number, upper: number){
    this.upperBound = upper;
    this.lowerBound = lower;
  }

  public check(n : number): boolean{
  //inclusive!
  return ( (n >= this.lowerBound) && (n <= this.upperBound));
}


public getRandom(){
    return Math.floor(Math.random()*(this.upperBound-this.lowerBound+1)+this.lowerBound);
  }

  public getRandomNonZero(){
    let n = 0;
    while(n==0){
      n = this.getRandom();
    }
    return n;
  }
}
