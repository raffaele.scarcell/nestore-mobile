import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterPage } from './register';
import { RecaptchaModule } from 'ng-recaptcha';

@NgModule({
  declarations: [
    RegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterPage),
    RecaptchaModule.forRoot(),
  ],
  exports: [
    RegisterPage,
    RecaptchaModule
  ]
})
export class RegisterPageModule { }
