# Nestore Mobile Application
Developed with [Ionic Framework](https://ionicframework.com/), you will find in this repository the code source of the mobile application of NESTORE, which it the main way for users to communicate with the NESTORE system. It includes, among others things, the communication with NESTORE chatbot.

## Version
The nestore-mobile project has been installed and tested on a MacBook Pro(16-inch, 2019) running the operating system macOs Big Sur 11.1 with the following environment :
 - WebStorm 2020.03.1
 - Android Studio 4.1.2
 - node.js v10.23.1
 - npm 6.14.10
 - cordova 9.0.0 (cordova-lib@9.0.1)
 - ionic 4.2.1 (included in package.json)
 - Gradle 6.8
 - JDK 1.8 (build 1.8.0_281-b09)

It is very important to set the environment variables correctly. These are the ones used onmy laptop :

![Environment variables used on MacOs](/README/path.png)

If it has not been done before, a version of Android SDK must be installed and in my case Iused the Android Level 27 version contained in Android Oreo (Version 8.1).

This can be done directly from Android Studio by going toPreferences -> Appearance  Behavior -> System Settings -> Android SDK and select theversion of Android desired.Through Android Studio you can create a virtual device, to do this just open the AVDManager (to do this from macos : menu help -> find action -> AVD Manager) and thencreate a virtual device with the version of Android Oreo and API Level 27.

## Development Environment
It is adviced to use [WebStorm](https://www.jetbrains.com/webstorm/).
  - Install [WebStorm](https://www.jetbrains.com/webstorm/)
 
Once installed :
  - Open Webstorm
  - Click on 'Check out from Version Control' -> Git
  <br/>
  <img src="/README/open_webstorm_printscreen.png" width="300">
  - Configure repository as on printscreen below.
  <br/>
  <img src="/README/configure_repository_printscreen.png" width="700">
  - Credentials are required, enter your mail and password. Warning : the password is not the same that for the connection on GitLab platform. Please setup a Personal Access Token (go to https://git.nestore-coach.eu/profile/personal_access_tokens and create one) and use that token value as password (and save the value in a safe place as you won't be able to see it again on UI!).
  <br/>
  <img src="/README/credentials_printscreen.png" width="400">

### Commands
```bash
npm install
```

Read [pre-requisites for ionic documentation](https://ionicframework.com/docs/intro/deploying/)
```bash 
ionic cordova build android
//debug mode - (only on android)
ionic cordova run android
```
The first execution will take time due to installations of plugins. 
A [problem of compatibility](https://github.com/fechanique/cordova-plugin-fcm/issues/481
), will give you an error for the plugin 'cordova-plugin-fcm-with-dependency-updated'. Once it happened, edit the file plugins/cordova-plugin-fcm/scripts/fcm_config_files_process.js and comment lines 44 and 80 with 'process.stdout.write(err);'. Then relaunch the command

If this [second problem](https://stackoverflow.com/questions/49208772/error-resource-androidattr-fontvariationsettings-not-found) of compatibility occurs,
open platforms/android/app/build.gradle and add :
```bash.
configurations.all {
        resolutionStrategy {
                force 'com.android.support:support-v4:27.1.1'
    }
} 
```

```bash.
ionic cordova run android
```

### Possible problems
An error that can occur is a problem with the SDK licenses. Even if all licenses at the timeof SDK installation have been accepted, there may be issues with Gradle. To fix it, I justfollowed the first answer of this post : https ://stackoverflow.com/questions/39760172.

To resolve this issue, I have executed the following command on the operating system MacOsBig Sur 11.1 : "~/Library/Android/sdk/tools/bin/sdkmanager –licenses" and then I haveaccepted the missing licenses.

If it has not been done before, a version of Android SDK must be installed and in my case Iused the Android SDK Platform 30 version contained in Android 11.0(R).

I had a problem with the Android version of the emulator that took me about 2 days ofwork. I was using an emulator with Android 11.0(R) and the Level 30 API and when I triedto change the content of the application it would not update in the emulator, to solve thisproblem I used a new virtual device with the version of Android Oreo 8.1.
