import {Component, Input, SimpleChanges, ViewChild} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { ChangeDetectorRef } from '@angular/core';

import {APIService} from '../../../chat/services/api';

@Component({
  selector: 'nestore-bmi',
  templateUrl: 'nestore-bmi.html'
})

export class NestoreBMIComponent {
  @ViewChild('bar') barElement;
  @Input() value = 35;
  thresholds = [{max:16, color: '#FD6E5B'},{max:18, color: '#FFD938', maxValid:true},{max:25, color: '#96E24B', maxValid:false},{max:35, color: '#FFD938',maxValid:false},{max:50, color:'#FD6E5B'}];
  currentThreshold;
  left = 100;
  position;
  previousThreshold;

  ngOnChanges(change){
    if(change.value){
      this.compute();
      this.ngAfterViewChecked();
    }
  }

  constructor(){
    this.compute();
  }

  compute(){
    for(let threshold of this.thresholds) {
      if (this.value < threshold.max || (this.value === threshold.max && threshold.maxValid!==undefined && !threshold.maxValid)) {
        this.currentThreshold = threshold;
        break;
      }
      else{
        this.currentThreshold = threshold;
      }
    }
    this.position = this.thresholds.indexOf(this.currentThreshold);
    console.log(this.position);
    this.previousThreshold = this.position>0?this.thresholds[this.position-1]:null;
  }

  ngAfterViewChecked(){
    if(this.barElement) {
      let previousThresholdMax = this.previousThreshold ? this.previousThreshold.max : 0;
      let segmentWidth = this.barElement.nativeElement.offsetWidth / this.thresholds.length;
      let leftInSegment = ((this.value - previousThresholdMax) / (this.currentThreshold.max - previousThresholdMax)) * segmentWidth;
      this.left = segmentWidth * (this.position) + leftInSegment;
    }
  }
}
