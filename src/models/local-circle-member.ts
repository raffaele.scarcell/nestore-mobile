export class LocalCircleMember {
  constructor(
    public user: string,
    public interactionsNumber: number,
    public interactionsProportion: number
  ) {}
}
