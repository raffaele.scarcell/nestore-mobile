// Make API requests typed
export interface INutritionEvent {
  domain: string;
  timestamp: number;
  id: string;
  dish: string,
  meal: string,
  hasPhoto: boolean
}

export interface IHistory {
  nutrition: Array<INutritionEvent>;
}

export interface IThreshold {
  id: string;
  nutritional_thresholds: Array<Object>;
}
