import {Component, Input, SimpleChanges} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import {LocalCircleInteractions} from "../../../models/local-circle-interactions";
/**
 * Generated class for the LocalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'nestore-local',
  templateUrl: 'local.html'
})
export class LocalComponent {
  showdetails = false;

  chartColors: string[] = [
    '#0B6789',
    '#FDB2A4',
    '#19A595',
    '#fdc63f',
    '#6ca537',
    '#c047fd',
    '#4257a5',
  ];

  chartDataSets: ChartDataSets[];
  chartLegend: boolean = false;
  chartLabels: string[][];
  chartType: ChartType = 'doughnut';
  chartOptions: ChartOptions = {
    animation: {duration:0},
    tooltips: {
      enabled: false
    },
    responsive: true,
    segmentShowStroke: false,
    cutoutPercentage: 70,
    elements: {
      arc: {
        borderWidth: 0,
      }
    },
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
          drawBorder: false
        },
        maintainAspectRatio: true,
      }],
      yAxes: [{
        ticks: {
          display: false,
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
      }],
    }
  };

  @Input() localCircleInteractions: any;

  constructor() {

  }

  ngOnChanges(change: SimpleChanges) {
    console.log('change', this.localCircleInteractions);
    if(!this.localCircleInteractions.error) {
      this.computeChart();
    }
  }

  computeChart() {
    let data = [];
    for(let i = 0; i < this.localCircleInteractions.local_circle_members.length; i++) {
      data.push(Math.round(this.localCircleInteractions.local_circle_members[i].interactions_proportion * 100));
    }
    this.chartDataSets = [
      {data: data, label: 'data1', fill: false, backgroundColor: this.chartColors},
    ];
  }

  convertHour(seconds: number): string {
    let hours =  Math.floor((seconds / 3600));
    return Math.floor(seconds / 3600) + 'h' + (seconds !== 0 ?('0'+Math.floor((seconds - hours*3600)/60)).slice(-2):'');
  }

}
