import {SimpleLimit} from './SimpleLimit';

export class CLimits {
  tileValuesLimits : SimpleLimit;
  mathOperationsLimits : SimpleLimit;

  constructor(lower: number, upper: number, mathOpLow: number, mathOpUp){
    this.tileValuesLimits = new SimpleLimit(lower, upper);
    this.mathOperationsLimits = new SimpleLimit(mathOpLow, mathOpUp);
  }

}
