import {ChangeDetectorRef, Component} from '@angular/core';
import {ChartOld} from "../chart";

@Component({
  selector: 'nestore-time-chart',
  templateUrl: 'nestore-time-chart.html'
})
export class NestoreTimeChart extends ChartOld{
  g;
  g2;
  g3;

  constructor(protected cdRef:ChangeDetectorRef){
    super(cdRef);
  }

  initChart() {
    if(this.g){
      this.g.remove();
    }
    if(this.g2){
      this.g2.remove();
    }
    if(this.g3){
      this.g3.remove();
    }

    this.g = document.createElementNS("http://www.w3.org/2000/svg", "g");

    let start = 7;
    let end = 100-7;
    let divisions = 3;
    let width = end-start;

    for(let i = 0; i<=divisions;i++) {
      let line1 = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "path"
      );

      let position = (start+i*(width/divisions));
      line1.setAttribute("d", "M"+position+"," + 0 + " L" + position + ", " + 30);
      line1.setAttribute("fill", "none");
      line1.setAttribute("stroke", "#F0F0F0");
      line1.setAttribute("stroke-width", "0.4");
      this.g.prepend(line1);

      let text = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "text"
      );

      text.setAttribute("x",""+position);
      text.setAttribute("y",35+"");
      text.setAttribute("font-size","4.5")
      text.setAttribute("text-anchor",'middle');
      text.setAttribute('font-family',"Lato-Black, Lato")
      text.setAttribute('font-weight',"800");
      text.setAttribute('fill', '#A8AAAC');
      text.innerHTML = this.time_convert(1200+i*(840/divisions));
      this.g.prepend(text);
    }

    this.svgChart.nativeElement.append(this.g);

    let startValue = this.data[0].startTime;
    let endValue=this.data[this.data.length-1].endTime;

    let startTime = 1200;
    let totalTime = 840;

    let value = start+width*(startValue-startTime)/totalTime;
    let graphWidth = (width/100)*(endValue-startValue)/totalTime;

    this.g2 = document.createElementNS("http://www.w3.org/2000/svg", "g");
    this.g3 = document.createElementNS("http://www.w3.org/2000/svg", "g");

    this.g2.setAttribute('clip-path','inset(0 0 0 0 round 80)');
    this.g2.setAttribute('transform','translate('+ value +' 13)');

    this.g3.setAttribute('transform','scale('+graphWidth+' 1)');

    for(let d of this.data) {
      let width = 100*(d.endTime-d.startTime)/(endValue-startValue);

      let offset = 100*(d.startTime-startValue)/(endValue-startValue);

      let rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
      rect.setAttribute('fill', d.color);
      rect.setAttribute('x', offset.toString());
      rect.setAttribute('width', width.toString());
      rect.setAttribute('height', '4');
      this.g3.append(rect);
    }

    this.g2.append(this.g3);
    this.svgChart.nativeElement.append(this.g2);

  }

  time_convert(num)
  {
    const hours = Math.floor(num / 60) % 24;
    const minutes = num % 60;
    return `${(hours.toString().padStart(2,'0'))}:${minutes.toString().padStart(2,'0')}`;
  }

}
