export class SocialIntegrationLoneliness {
  constructor(
    public number_of_interaction: number,
    public in_person: number,
    public other_type: number
  ) {}
}
