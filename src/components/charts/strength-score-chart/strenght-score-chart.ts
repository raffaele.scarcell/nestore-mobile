import {Component, Input, SimpleChanges} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import {APIService} from '../../../chat/services/api';

@Component({
  selector: 'nestore-strenght-score-chart',
  templateUrl: 'strenght-score-chart.html'
})
export class StrengthScoreChartComponent {
  @Input() timeframe;
  @Input() data;
  @Input() total;

  value;
  ngOnChanges(change){
    if(change.timeframe){
      if(this.timeframe==='week'){
        this.total = 50;
      }
      else if(this.timeframe==='month'){
        this.total = 200;
      }
    }
    if(change.data) {
      if(!this.data.strength_unstructured)
      {
        this.data.strength_unstructured=0;
      }
      if(!this.data.strength_structured)
      {
        this.data.strength_structured=0;
      }
      this.value = (this.data.strength_structured+this.data.strength_unstructured).toFixed(1);

    }
  }
}
