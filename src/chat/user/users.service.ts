import { Injectable } from '@angular/core';
import { User } from './user.model';
import {BehaviorSubject, Subject} from 'rxjs/index';


/**
 * UserService manages our current user
 */
@Injectable()
export class UsersService {

}

export const userServiceInjectables: Array<any> = [
  UsersService
];
