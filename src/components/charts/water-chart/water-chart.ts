import {Component, Input} from '@angular/core';

@Component({
  selector: 'nestore-water-chart',
  templateUrl: 'water-chart.html'
})
export class WaterChartComponent {
  @Input() timeframe;
  @Input() value = 0;
  @Input() total = 1600;
}
