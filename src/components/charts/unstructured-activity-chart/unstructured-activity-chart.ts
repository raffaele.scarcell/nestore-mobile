import {Component, Input, SimpleChanges} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import {APIService} from '../../../chat/services/api';

@Component({
  selector: 'nestore-unstructured-activity-chart',
  templateUrl: 'unstructured-activity-chart.html'
})
export class UnStructuredActivityChartComponent {
  @Input() timeframe;
  @Input() data;
  showdetails = false;

  constructor() {

  }

  ngOnChanges(change) {
    if(change.data){
      if(this.data && this.data.distance) {
        this.data.distance = Math.round(this.data.distance * 100) / 100;
      }
    }
  }
}
