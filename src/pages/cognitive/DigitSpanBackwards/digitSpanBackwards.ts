import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AlertController} from 'ionic-angular';
import {TranslateService} from '@ngx-translate/core';
import {spanDigitGenerator} from './spanDigitGenerator';
import {APIService} from "../../../chat/services/api";
import {Insomnia} from "@ionic-native/insomnia";

@Component({
  selector: 'digitspan-game',
  templateUrl: 'digitSpanBackwards.html',
})
export class DigitSpanBackwardsComponent {
  readonly MAX_BLOCK = 2;
  readonly MAX_LEVEL = 8;
  @Output() finish = new EventEmitter();
  @Output() onSave = new EventEmitter();

  @Input() options;

  block = 1;
  level = 2;
  correctAnswer = false;

  private smallLoaded = true;
  private enableKeyboard = false;
  private randomList;
  private currentPosition = null;
  private hideNumber = true;
  private canAnswer = false;
  private initialInterval = 2500; //milliseconds
  private operationsInterval = 2500; //milliseconds
  private inISI = 500;// milliseconds InterStimulus Interval (in between math operations)

  trialStep = 1;
  help = true;
  tuto = false;
  replies = [];

  constructor(public api: APIService, public alertController: AlertController, public translate: TranslateService, private insomnia: Insomnia){
    this.randomList = new spanDigitGenerator().getList(2);
  }

  ngOnInit(){
    this.insomnia.keepAwake().then(
      () => console.log('keep awake success'),
      () => console.log('keep awake error')
    );
  }

  ngOnChanges(change){
    if(change.options && this.options) {
      console.log(this.options);
    }
  }

  start(){
    this.displayCross();
  }

  displayCross(){
    this.currentPosition = null;
    this.hideNumber = true;
    setTimeout(() => {
      this.hide()
    }, this.operationsInterval);
  }


  runISIdelay() {
    this.hideNumber = false;
    this.canAnswer = true;
    setTimeout(() => {
      this.hide()
    }, this.inISI);
  }

  hide() {
    this.hideNumber = true;
    if(this.currentPosition===this.randomList.length-1){
      setTimeout(() => {
        this.enableKeyboard = true;
      },this.initialInterval);
    }
    else {
      if(this.currentPosition===null){
        this.currentPosition = 0;
      }
      else{
        this.currentPosition++;
      }
      console.log(this.currentPosition);
      this.hideNumber=true;
      setTimeout(() => {
        this.runISIdelay()
      }, this.initialInterval);
    }
  }

  validate(value){
    this.enableKeyboard = false;
    let correct = this.isCorrect(value);
    console.log(correct);
    this.correctAnswer = this.correctAnswer || correct;
    for(let l = 0; l<this.level;l++) {
      this.replies.push({'task-version': this.level, blockNumber: this.block, trialNumber: l, givenResponse: parseInt(value[l]), correctResponse: this.randomList[this.randomList.length-1-l], points:parseInt(value[l])===this.randomList[this.randomList.length-1-l]?1:0})
    }
    console.log(this.replies);
    if((this.block<this.MAX_BLOCK)){
      this.block++;
      this.randomList = new spanDigitGenerator().getList(this.level);
      this.displayCross();
    }
    else if(this.block===this.MAX_BLOCK && this.correctAnswer && this.level <= this.MAX_LEVEL){
      this.block = 1;
      this.level++;
      this.correctAnswer = false;
      this.randomList = new spanDigitGenerator().getList(this.level);
      this.displayCross();
    }
    else{
      this.help = true;
      this.hideNumber = true;
      this.save().then();
    }
  }

  isCorrect(value) {
    console.log('value:' + value);
    try {
      let index = 0;
      for (let c of this.randomList) {
        if (c !== parseInt(value[this.randomList.length-1-index])) {
          return false;
        }
        index++;
      }
      return true;
    } catch (e) {
      return false;
    }
  }

  save() {

    let gameState = {};
    gameState['task_name'] = 'Digit Span';
    gameState['module_name'] = 'Daily';
    gameState['replies'] = this.replies;
    gameState['score'] = this.level-1;

    this.smallLoaded = false;
    return this.api.postGame(gameState).then(()=> {
        this.smallLoaded = true;
        this.onSave.emit({messageId: this.options.messageId, messageIndex: this.options.messageIndex, score: 1 + this.level - (this.correctAnswer ? 1 : 2)});
      }
    ).catch(()=>{
        this.smallLoaded = true;
      }
    )// todo: loading during sending
  }


  trialNext() {
    this.trialStep++;
    if (this.trialStep == 2 && !this.tuto) {
      this.help = false;
      this.start();
    }
    if(this.trialStep==3 && !this.tuto){
      this.help = false;

      this.finish.emit();
    }
  }

  ngOnDestroy(){
    this.insomnia.allowSleepAgain().then(
      () => console.log('allow sleep again success'),
      () => console.log('allow sleep again error')
    );
  }



}
