export class Util {

  public static setProfile(profile, partialProfile){
    for(let key of Object.keys(partialProfile)){
      profile[key] = partialProfile[key];
    }
  }

  public static compareVersion(version1, version2){
    try{
      let array1 = version1.split('.');
      let array2 = version2.split('.');
      let index = 0;
      for(let el of array1){
        if(parseInt(el)>parseInt(array2[index])) {
          return 1
        }
        if(parseInt(el)<parseInt(array2[index])){
          return -1
        }
        index++;
      }
      return 0;
    }
    catch (e) {
      return null
    }
  }
}
