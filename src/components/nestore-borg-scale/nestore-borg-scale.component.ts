import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'nestore-borg-scale',
  templateUrl: 'nestore-borg-scale.component.html',
})
export class NestoreBorgScaleComponent {
  @Output() valid = new EventEmitter();
  value = 13;
  @Input() type = 'borg';

  validate(){
    this.valid.emit(this.value);
  }
}
