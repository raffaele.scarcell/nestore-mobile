import {ChangeDetectorRef, Component, Input, SimpleChanges} from '@angular/core';

@Component({
  selector: 'nestore-sleep-chart',
  templateUrl: 'sleep-chart.html'
})
export class SleepChartComponent{
  @Input() timeframe = 'day';
  @Input() data;
  showdetails = false;
  d = [];
  d1 = [];
  d2 = [];

  totalSleep;
  totalAwake;
  quality;

  ngOnInit(){
    if(this.timeframe==='day') {
      let out = [];
      for (let i = 0; i <= this.data.details.length - 1; i++) {
        console.log(this.data.details[i]);
        if (this.data.details[i].type === 'OUT_OF_BED') {
          out.push(this.data.details[i]);
        } else {
          break;
        }
      }
      for (let i = this.data.details.length - 1; i > 0; i--) {
        console.log(this.data.details[i]);
        if (this.data.details[i].type === 'OUT_OF_BED') {
          out.push(this.data.details[i]);
        } else {
          break;
        }
      }
      console.log(out);
      for (let o of out) {
        this.data.details.splice(this.data.details.indexOf(o), 1);
      }
      console.log(this.data);

      this.d = this.data.details.map((x) => {
        let obj = {};
        let start = new Date(x.start_time);
        let end = new Date(x.end_time);
        obj['startTime'] = this.getMinutes(start);
        obj['endTime'] = this.getMinutes(end);
        obj['color'] = x.type === 'SHALLOW_SLEEP' || x.type === 'DEEP_SLEEP' || x.type == 'REM' ? '#0B6789' : '#FDB2A4';
        return obj;
      });

      if(this.data.summary) {
        this.totalSleep = this.secondsToHoursAndMinutes(this.data.summary.time_asleep);
        this.totalAwake = this.secondsToHoursAndMinutes(this.data.summary.time_awake)
      }
    }
    else if(this.timeframe==='week'){
      this.d = this.data.details.map((x) => {
        if(!x) {
          return null;
        }
        let obj = {};
        let start = new Date(x.start_time);
        let end = new Date(x.end_time);
        obj['startValue'] = this.getMinutes(start);
        obj['endValue'] = this.getMinutes(end);
        return [obj['endValue']-obj['startValue'],this.getMinutes(start)-1200];
      });
      this.d1 = this.d.map((e)=>e==null?e:e[0]);
      this.d2 = this.d.map((e)=>e==null?e:e[1]);

      this.totalSleep = this.secondsToHoursAndMinutes(this.data.summary.time_asleep);
      console.log(this.totalSleep);
      this.quality = this.data.summary.quality;
    }

    else if(this.timeframe==='month'){
      this.d = this.data.details;
      this.totalSleep = this.secondsToHoursAndMinutes(this.data.summary.time_asleep);
      this.quality = this.secondsToHoursAndMinutes(this.data.summary.quality)
    }

    console.log(this.d);


  }


  getMinutes(start){
    let c = (start.getMinutes() + (60 * start.getHours()));
    if(c<1000){
      c+=1440;
    }
    return c;
  }

  secondsToHoursAndMinutes(seconds){
    return {hours: seconds===null?0:Math.round( seconds/ 60.0), minutes: seconds===null?0:('0'+(seconds%60)).toString().slice(-2)}
  }

}
