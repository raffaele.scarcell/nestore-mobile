import {User} from '../user/user.model';
import {uuid} from './../util/uuid';

export class Message {

  id: string;
  sentAt: Date;
  isRead: boolean;
  author;
  text;
  image: any;
  type: string;
  action: string;
  choice: string;
  imagedate;
  bot = false;
  isTyping = false;
  timeBeforeDisplay = 0;
  canWrite = true;
  discussionEnd = false;
  mode : string;
  answered = false;
  intent;
  previous = null;
  profile = null;
  data = null;
  index = 0;

  constructor(obj?: any) {
    this.id = obj && obj.id || uuid();
    this.index = obj && obj.index || 0;
    this.isRead = obj && obj.isRead || false;
    this.sentAt = obj && obj.sentAt || new Date();
    this.author = obj && obj.author || null;
    this.text = obj && obj.text || null;
    this.image = obj && obj.image || null;
    this.type = obj && obj.type || null;
    this.action = obj && obj.action || null;
    this.choice = obj && obj.choice || null;
    this.bot = obj && obj.bot || null;
    this.isTyping = obj && obj.isTyping || null;
    this.timeBeforeDisplay = obj && obj.timeBeforeDisplay || 0;
    this.canWrite = obj && obj.canWrite || false;
    this.discussionEnd = obj && obj.discussionEnd || 0;
    this.mode = obj && obj.mode || null;
    this.intent = obj && obj.intent || null;
    this.previous = obj && obj.previous || null;
    this.profile = obj && obj.profile || null;
    this.data = obj && obj.data || null
  }
}
