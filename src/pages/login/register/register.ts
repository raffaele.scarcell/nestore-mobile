import { Component, NgZone, Output, EventEmitter } from "@angular/core";
import { IonicPage, ViewController, AlertController, ToastController } from "ionic-angular";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { WheelSelector } from "@ionic-native/wheel-selector";

import { birthdayDates, genders, suffixes, titles, countries, timezones, languages } from "./pickers";
import { GOOGLE_CAPTCHA_KEY, REGISTRATION_FORM_ACTIVE } from "./REGISTER_CONFIG";
import { LoginRegisterProvider } from "../login-register/login-register";
import { NavController } from "ionic-angular/navigation/nav-controller";

@IonicPage()
@Component({
  selector: "page-register",
  templateUrl: "register.html"
})
export class RegisterPage {
  @Output() quit = new EventEmitter();

  public validation_messages = {
    userName: [{ type: "required", message: "Username is required." }],
    firstName: [{ type: "required", message: "Name is required." }],
    lastName: [{ type: "required", message: "Last name is required." }],
    screenName: [{ type: "required", message: "Screen name is required." }],
    email: [{ type: "required", message: "Email is required." }, { type: "pattern", message: "Please wnter a valid email." }],
    password: [
      { type: "required", message: "Password is required." },
      { type: "minlength", message: "Password must be at least 5 characters long." },
      { type: "pattern", message: "Your password must contain at least one uppercase, two lowercase, and two number." }
    ],
    verifyPassword: [{ type: "required", message: "Confirm password is required." }],
    matching_passwords: [{ type: "areEqual", message: "Password mismatch." }]
  };

  private checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.verifyPassword.value;

    return pass === confirmPass ? null : { areEqual: true };
  }

  public REGISTER_ACTIVES = REGISTRATION_FORM_ACTIVE;

  public password_group: FormGroup = this.formBuilder.group(
    {
      password: ["", [Validators.required, Validators.minLength(8), Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*?[0-9].*?[0-9])[a-zA-Z0-9]+$")]],
      verifyPassword: ["", [Validators.required, Validators.minLength(8), Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*?[0-9].*?[0-9])[a-zA-Z0-9]+$")]]
    },
    { validator: this.checkPasswords }
  );
  public signUpForm: FormGroup = this.formBuilder.group({
    userName: ["", [Validators.required, Validators.minLength(6)]],
    firstName: ["", [Validators.required]],
    lastName: ["", [Validators.required]],
    email: ["", [Validators.required, Validators.email]],
    password_group: this.password_group,
    screenName: ["", [Validators.required]],
    birthday: [null],
    country: [null],
    gender: [null],
    defaultTimeZone: [null],
    description: [null],
    language: [null],
    title: [null],
    middleName: [null],
    suffix: [null],
    job: [null],
    pilotSite: [null]
  });
  public GOOGLE_KEY: string = GOOGLE_CAPTCHA_KEY;
  private captchaResponse: string;
  public captchaPassed: boolean = false;
  public genders = genders;
  public titles = titles;
  public suffixes = suffixes;
  public countries = countries;
  public timezones = timezones;
  public languages = languages;

  constructor(
    private zone: NgZone,
    public formBuilder: FormBuilder,
    private viewCtrl: ViewController,
    private alertCtrl: AlertController,
    private selector: WheelSelector,
    private toastCtrl: ToastController,
    private auth: LoginRegisterProvider,
    private navCtrl: NavController
  ) {}

  public openPicker(name: string, objectType: any = "default") {
    this.selector
      .show({
        title: ``,
        items: objectType == "birthdates" ? [birthdayDates.day, birthdayDates.month, birthdayDates.year] : [objectType.prop],
        positiveButtonText: "Select",
        negativeButtonText: "Cancel",
        defaultItems:
          objectType == "birthdates"
            ? [{ index: 0, value: birthdayDates.day[0].description }, { index: 1, value: birthdayDates.month[0].description }, { index: 2, value: birthdayDates.year[70].description }]
            : [{ index: 0, value: objectType.prop[0].description }]
      })
      .then(
        result => {
          let msg = objectType == "birthdates" ? `Selected ${result[0].description}-${result[1].description}-${result[2].description}` : `Selected ${result[0].description}`;
          let toast = this.toastCtrl.create({
            message: msg,
            duration: 4000
          });
          toast.present();
          objectType == "birthdates"
            ? this.signUpForm.get("birthday").setValue(`${result[2].description}-${result[1].description}-${result[0].description}`)
            : this.signUpForm.get(name).setValue(result[0].description);
        },
        err => console.log("Error: ", err)
      );
  }

  captchaResolved(response: string): void {
    this.zone.run(() => {
      this.captchaPassed = true;
      this.captchaResponse = response;
    });
  }

  public signUp(): void {
    let data = {
      captchaResponse: this.captchaResponse,
      form: this.signUpForm.controls
    };

    this.auth.register(data).subscribe(
      result => {
        let alert = this.alertCtrl.create({
          title: `<center> Thank you for registering </center>`,
          buttons: [
            {
              text: "Ok",
              handler: () => {
                this.quit.next();
              }
            }
          ]
        });
        alert.present();
      },
      err => {
        let alert = this.alertCtrl.create({
          title: `<center> There was an error </center>`,
          buttons: [
            {
              text: "Ok",
              handler: () => {}
            }
          ]
        });
        alert.present();
      }
    );
  }
}
