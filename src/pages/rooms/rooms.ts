import {Component, EventEmitter, Output} from "@angular/core";
import {APIService} from "../../chat/services/api";
import {GlobalvarProvider} from "../../app/globalprovider";

@Component({
  selector: 'nestore-rooms',
  templateUrl: 'rooms.html',
})
export class Rooms {

  constructor(public api: APIService, public globalProvider: GlobalvarProvider){

  }
  @Output() back = new EventEmitter();
  loaded = false;
  data;
  rooms = ['bedroom', 'kitchen', 'living', 'bathroom'];

  ngOnInit(){
    this.loaded = false;
    this.api.getRooms().then((res)=>{
      this.data = res;
      this.loaded = true;
    }).catch((res)=>{
        console.log(res);
        this.loaded = true;
        this.data = null;
      }
    )
  }
}
