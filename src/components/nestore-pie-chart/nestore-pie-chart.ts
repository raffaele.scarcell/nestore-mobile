import { Component, Input, Renderer2, ElementRef } from '@angular/core';
import { Threshold } from '../../models/threshold';


@Component({
  selector: 'nestore-pie-chart',
  templateUrl: 'nestore-pie-chart.html'
})
export class NestorePieChartComponent {

  @Input() value = 0;
  @Input() color = '#76C1DB';
  @Input() text1 ;
  @Input() text2 ;
  @Input() thickness = 6;

  v=0;
  cx;cy;
  constructor() {

  }

  ngOnChanges(changes){
    if(changes.value){
      this.v = this.value;
      if(this.value>100){
        this.v=100;
      }
      this.cx = (16+this.thickness/2)+16*Math.sin(Math.PI/180*(this.v/100)*360);
      this.cy = (16+this.thickness/2)+16*-Math.cos(Math.PI/180*(this.v/100)*360);
    }
  }

}
