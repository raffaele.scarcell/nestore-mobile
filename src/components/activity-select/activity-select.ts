import {Component, EventEmitter, Input, Output} from '@angular/core';

import { APIService } from '../../chat/services/api';
import {GlobalvarProvider} from "../../app/globalprovider";

@Component({
  selector: 'activity-select',
  templateUrl: 'activity-select.html',
})
export class ActivitySelectComponent {
  @Output() onClose = new EventEmitter();
  @Output() valid = new EventEmitter();

  suggestions = [];
  loaded;
  mode = 'normal';
  selected;

  constructor(public api: APIService,  private globalvarProvider: GlobalvarProvider) {

  }

  ngOnInit(){
    this.loaded = false;
    this.api.getUnstructuredWorkouts().then((res)=>{
      this.suggestions = res;
      this.loaded = true;
    })

  }

}
