import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'nestore-multichoice',
  templateUrl: 'nestore-multichoice.component.html',
})
export class NestoreMultichoiceComponent {
  @Input() choices = [];
  @Input() canEdit = true;
  @Output() validated = new EventEmitter();

  selected = [];

  validate() {
    let text = "";
    let intents = [];
    let selectedChoices = this.choices.filter((x)=>x.selected);
    for(let choice of selectedChoices){
        text += choice.text;
        intents.push(choice.intent);
        if(choice != selectedChoices[selectedChoices.length-1]){
          text += ", ";
        }
    }
    this.validated.emit({text:text, intent: intents});
  }

}
