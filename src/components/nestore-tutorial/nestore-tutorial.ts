import {Component} from "@angular/core";
import {GlobalvarProvider} from "../../app/globalprovider";
import {TutorialService} from "../../providers/tutorialService";

@Component({
  selector: 'nestore-tutorial',
  templateUrl: 'nestore-tutorial.html'
})
export class NestoreTutorialComponent {
  constructor(public globalvarProvider: GlobalvarProvider, public tutorialService: TutorialService){

  }

  clickTutorialText(){
    this.tutorialService.nextTip();
  }
}
