import {Component, Input, SimpleChanges} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import {APIService} from '../../../chat/services/api';
import {Threshold} from "../../../models/threshold";

@Component({
  selector: 'nestore-nutrient-chart',
  templateUrl: 'nutrient-chart.html'
})
export class NutrientChartComponent {
  @Input() thresholds;

  private getIdForTranslation(str){ // TODO: ASK SIVLIA TO USE KEY FOR nutrients
    return (str.charAt(0).toLowerCase() + str.slice(1)).replace(/\s+/g, '');
  }

}


