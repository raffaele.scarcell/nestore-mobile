import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {Keyboard} from '@ionic-native/keyboard';
import { APIService } from '../../chat/services/api';


@Component({
  selector: 'meal-picker-search',
  templateUrl: './meal-picker-search.component.html',
})
export class MealPickerSearchComponent {
  @ViewChild('input1') input1;

  @Input() placeholder;

  @Output() isSearching = new EventEmitter();
  @Output() resultsChanges = new EventEmitter();
  @Output() valid = new EventEmitter();
  @Output() cancel = new EventEmitter();

  list = [];
  found = [];
  text = '';
  selected = false;
  hasKeyboard = false;

  constructor(public apiService: APIService, public keyboard: Keyboard){

    keyboard.onKeyboardShow().subscribe(()=>{this.hasKeyboard = true; this.isSearching.emit(this.hasKeyboard)});
    keyboard.onKeyboardHide().subscribe(()=>{this.hasKeyboard = false; this.isSearching.emit(this.hasKeyboard)});

    apiService.groupedFoodList().then(res=> {
      this.list=res.food.names;
      this.found=this.list.slice(0,4);
      if(this.text!=''){
        this.sort();
      }
    });
  }

  affectText(t){
    this.text=t;

    if(document.hasFocus())
      setTimeout(()=> {
        this.input1.nativeElement.blur();
        this.selected=true;
      },1)
  }

  validate(){
    this.valid.emit({meal:this.text, isInList:this.isInList(this.text)});
    this.selected=false;
  }

  sort(){
    console.log("calling sort()");
    let filteredList = this.list.filter(res=>
    {
      let condition = true;
      for(let searchword of this.text.split(' ')) {
        let condition2 = false;
        for (let word of res.split(' ')) {
          if (word.toUpperCase().startsWith(searchword.toUpperCase())) {
            condition2 = true;
            break;
          }
        }
        condition = condition && condition2;
        if(!condition){
          return false;
        }
      }
      return true;});

    filteredList=filteredList.sort((a,b) =>{
      if(a.toUpperCase().startsWith(this.text.toUpperCase()) || !b.toUpperCase().startsWith(this.text.toUpperCase())){
        return -1;
      }
      return 0;
    });

    this.found=filteredList.slice(0,4);
    this.resultsChanges.emit({text: this.text, items: this.found});
  }


  isInList(value){
    return this.list.includes(value)
  }
}
