import {Component} from '@angular/core';
import {GlobalvarProvider} from '../app/globalprovider';
import {TranslateService} from '@ngx-translate/core';
import {APIService} from '../chat/services/api';
import {TabsPage} from '../pages/tabs/tabs';

@Component({
  selector: 'physical-prefrences-choice',
  templateUrl: 'physical-preferences-choice.component.html',
})
export class PhysicalPreferencesChoiceComponant {
 step = 0;
 currentIndex=0;
 loaded = false;
 data;
 choice = [];
 selectedIntensity = null;
 selectedSession = null;


 constructor(public api: APIService, public globalvar : GlobalvarProvider, translate: TranslateService){

   this.choice['vigorous'] = [];
   this.choice['moderate'] = [];
   this.choice['light'] = [];


     this.loaded = true;
   this.loaded = false;
   this.api.getPossibleSessions(this.globalvar.profile.user_id).then((res)=>{
     this.data = res;

     for(let entry of res){
       this.choice[entry.intensity].push(entry.sessions)
     }
     console.log(this.choice);
     this.loaded = true;

   }).catch(()=>{
     //TODO: manage error.
   });
 }

  validateIntensity(selected){
    this.selectedIntensity = selected;
    this.step++;
  }

  validate(session){
   this.selectedSession = session;
   this.step++;
  }

  accept(){
   this.loaded = false;
   this.api.postStucturedPreferences(this.globalvar.profile.user_id, {'intensity':this.selectedIntensity, sessions:this.selectedSession}).then(()=>{
     this.api.initUser().then(
       (res)=>{
         this.globalvar.profile = res;
         this.loaded = true;
       }
     );
   });
  }

  clickCancel(){
   this.step = 0;
  }

}
