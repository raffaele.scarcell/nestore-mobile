import {Component} from '@angular/core';
import {GlobalvarProvider} from '../app/globalprovider';
import {TranslateService} from '@ngx-translate/core';
import {APIService} from '../chat/services/api';

@Component({
  selector: 'pathway-choice',
  templateUrl: 'pathway-choice.component.html',
})
export class PathwayChoiceComponent {
 step = 0;
 currentIndex=0;
 loaded = false;
 data;
 choice = [];
 selected = null;

 message = null;

 constructor(public api: APIService, public globalvar : GlobalvarProvider, translate: TranslateService){
   this.loaded = false;
    this.api.getPossiblePathways(this.globalvar.profile.user_id).then((res)=>{
      this.loaded = true;
      this.data = res;
    }).catch((e)=>{
      this.globalvar.internalError = {text:'Pathway choice error'};
    });
 }

 choosePathway(domain,pathway){
   this.selected = null;
   if(this.currentIndex<this.data.length){
     this.choice.push({'pathway-name':pathway, 'domain-name':domain});
    this.currentIndex++;
   if(this.currentIndex===this.data.length) {
      this.step = 2;
      this.startHAPAQuestionnaire();
     }
   }
 }

 confirm(){
   this.loaded = false;
   this.api.postCurrentPathways(this.globalvar.profile.user_id, this.choice).then(()=>{
     this.api.initUser().then(
       (res)=>{
         this.globalvar.profile = res;
         this.loaded = true;
       }
     );
   }).catch((e)=>{console.log(e)});
 }

 back(){
   if(this.step>=2){
     this.message = null;
     this.step--;
   }
   if(this.step==1) {
     if (this.currentIndex > 0) {
       this.currentIndex--;
       this.choice.pop();
     }
     else{
       this.step--;
     }
   }

   if(this.step==2) {
     this.startHAPAQuestionnaire();
   }
 }

 cancel(){
   this.choice=[];
   this.currentIndex = 0;
   this.step = 0;
   this.message=null;
 }

  validate(pathway){
    this.selected = pathway;
  }

  startHAPAQuestionnaire(){
   this.api.startQuestionnaire('HAPA').then((message)=>{
    this.message=message;
     this.message.bot = true;
   })
  }

  sendText(event): void {
   console.log('send');
   this.api.postQuestionnaireAnswer({
     questionnaireId: 'HAPA',
     message: {text:event.text}
   }).then((message)=>{
     this.message=message;
     this.message.bot = true;
     console.log('test');
     if(this.message.discussionEnd){
       setTimeout(()=>{
         this.step++;
       },2000)
     }
   });
  }

}
