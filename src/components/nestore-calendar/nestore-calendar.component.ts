import {Component, EventEmitter, Input, Output} from '@angular/core';

enum CalendarMode {
  Week = "week",
}

@Component({
  selector: 'nestore-calendar',
  templateUrl: './nestore-calendar.component.html',

})

export class NestoreCalendarComponent {
  @Input() disableAfterToday  = true;
  @Input() disableBeforeToday  = true;
  @Input() mode : CalendarMode = CalendarMode.Week;
  @Input() date = new Date();
  @Output() cancel = new EventEmitter();
  @Output() dateChange = new EventEmitter();

  selectedDate;
  monthArray;
  dayNameArray;
  today;

  constructor() {
    this.today = new Date();
    let baseDate = new Date(Date.UTC(2017,0, 1)); // just a Monday
    this.dayNameArray = [];
    for(let i = 0; i < 7; i++)
    {
      this.dayNameArray.push(baseDate);
      baseDate = new Date(baseDate.setDate(baseDate.getDate() + 1));
    }
    console.log(this.dayNameArray)
  }


  ngOnChanges(change){
    if(change.date){
      this.selectedDate = new Date(this.date);
      this.initMonth();
    }
  }

  initMonth(){
    this.monthArray = [[]];
    console.log(this.selectedDate);
    let numberOfDays = this.nbDaysInMonth(this.selectedDate.getFullYear(),this.selectedDate.getMonth());
    for(let i = 1; i <= numberOfDays; i++){
      let date  = new Date(this.selectedDate.getFullYear(), this.selectedDate.getMonth(), i);
      if(i===1){
        for(let j = 1; j < (date.getDay() === 0 ? 7 : date.getDay());j++) {
          this.monthArray[this.monthArray.length - 1].push(null);
        }
      }
      if(this.selectedDate.getDate() === date.getDate() && this.selectedDate.getMonth() === date.getMonth() && this.selectedDate.getFullYear() === date.getFullYear()){
        date = this.selectedDate;
      }
      this.monthArray[this.monthArray.length-1].push(date);
      if(date.getDay()===0){
        this.monthArray.push([]);
      }
    }
  }

  clickCancel(){
    this.cancel.emit();
  }

  clickOK(){
    this.date = this.selectedDate;
    this.dateChange.emit(this.date);
    this.cancel.emit();
  }

  switchDate(direction, timeframe, date) {
    if (direction == "back") {
      switch (timeframe) {
        case "day":
          date.setDate(date.getDate() - 1);
          break;
        case "week":
          date.setDate(date.getDate() - 7);
          break;
        case "month":
          date.setMonth(date.getMonth() - 1);
          break;
      }
    }
    else {
      switch (timeframe) {
        case "day":
          date.setDate(date.getDate() + 1);
          break;
        case "week":
          date.setDate(date.getDate() + 7);
          break;
        case "month":
          date.setMonth(date.getMonth() + 1);
          break;
      }
    }

    this.selectedDate = new Date(date);
    this.initMonth();
  }

  areSameMonth(date1, date2){
    return date1.getFullYear() === date2.getFullYear() && date1.getMonth() === date2.getMonth();
  }

  nbDaysInMonth (year, month) {
    return new Date(year, month+1, 0).getDate();
  }

  selectDate(day){
    if(
      this.disableAfterToday == false || (this.disableAfterToday && day <= this.today)) {
      this.selectedDate = day;
    }
  }
}
